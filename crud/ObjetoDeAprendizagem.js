const executaQuery = require('../database/queries_sints')

class ObjetoDeAprendizagem {
  lista() {
    const sql = 'SELECT * FROM objeto_aprendizagem'

    return executaQuery(sql).then(dados => {
        const objetos = dados
    
        return objetos.map(objeto => ({
          id: objeto.id,
          tema: objeto.tema,
          dtdispo: objeto.data,
          finalidade: objeto.finalidade,
          atividade: objeto.atividade,
          dplataf: objeto.disp_telessaude,
          dares: objeto.disp_ares, 
          url_ares: objeto.url_ares, 
          url_youtube: objeto.url_youtube, 
          davasus: objeto.disp_avasus, 
          drsociais: objeto.disp_rede_social, 
          doutros: objeto.disp_outros, 
          tipo: objeto.tipo, 
          decs: objeto.tema_decs, 
          num: objeto.num_acesso
          
        }))
    })
  }

  
}

module.exports = new ObjetoDeAprendizagem
