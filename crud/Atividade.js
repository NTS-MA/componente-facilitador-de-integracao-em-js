const executaQuery = require('../database/queries_sints')
//const conexao = require('../conexao_sints')

class Atividade {
  lista(params) {

    const sql = `SELECT DISTINCT atividade.id AS id, CONCAT( atividade.dt, ' ', atividade.hr_inicio ) AS dtdispo, FORMAT(FLOOR( TIME_TO_SEC(TIMEDIFF(atividade.hr_termino , atividade.hr_inicio))/60),0) AS cargah, atividade.hr_inicio, atividade.hr_termino, tipo_nt4.id AS tipo, atividade.cod_decs AS decs FROM presenca INNER JOIN pessoa ON presenca.pessoa = pessoa.id INNER JOIN prof_geral ON prof_geral.pessoa = presenca.pessoa INNER JOIN atividade ON presenca.atividade = atividade.id LEFT JOIN avaliacoes ON presenca.id = avaliacoes.presenca LEFT JOIN prof_saude ON prof_saude.pessoa = presenca.pessoa LEFT JOIN tipo ON atividade.tipo = tipo.id LEFT JOIN tipo_nt4 ON tipo.tipo_nt4 = tipo_nt4.id WHERE atividade.dt != '' AND atividade.dt BETWEEN '${params.dt_i}' AND '${params.dt_f}' ORDER BY atividade.id DESC; SELECT atividade.id AS id_atividade, CONCAT( atividade.dt, ' ', atividade.hr_inicio ) AS dtparti, equipe.ubs AS cnes, avaliacoes.pergunta AS satisf, prof_geral.cbo AS cbo, pessoa.cpf AS cpf, pessoa.id as id, equipe.codigo_ine AS ine FROM presenca INNER JOIN pessoa ON presenca.pessoa = pessoa.id INNER JOIN prof_geral ON prof_geral.pessoa = presenca.pessoa INNER JOIN atividade ON presenca.atividade = atividade.id LEFT JOIN avaliacoes ON presenca.id = avaliacoes.presenca LEFT JOIN prof_saude ON prof_saude.pessoa = presenca.pessoa LEFT JOIN equipe ON prof_saude.equipe = equipe.id LEFT JOIN ubs ON equipe.ubs = ubs.cnes LEFT JOIN tipo ON atividade.tipo = tipo.id LEFT JOIN tipo_nt4 ON tipo.tipo_nt4 = tipo_nt4.id`

    return executaQuery(sql).then(dados => {
      const atividades = dados[0]
      const participantes = dados[1]

      return atividades.map(atividades_teleeducacao => {
        
        const participantesAtividade = participantes.filter(participante => participante.id_atividade === atividades_teleeducacao.id)
        console.log(participantesAtividade)
        console.log('coéeeeeeeeeeeeeeeeeeeeeeeeeeeee')
        return ({
            id: atividades_teleeducacao.id,
            dtdispo: atividades_teleeducacao.dtdispo,
            origemf: "000",
            hr_inicio: atividades_teleeducacao.hr_inicio,
            hr_termino: atividades_teleeducacao.hr_termino,
            tipo: atividades_teleeducacao.tipo,
            decs: atividades_teleeducacao.decs,
            //participantes: atividadeParticipantes
            cargah: atividades_teleeducacao.cargah,
            participacoes_teleeducacao: participantesAtividade
        })
      })
    })
  }

}

module.exports = new Atividade


/* 

const executaQuery = require('../database/queries_sints')
//const conexao = require('../conexao_sints')

class Atividade {
  lista(params) {

    const sql = `SELECT DISTINCT atividade.id AS id, CONCAT( atividade.dt, ' ', atividade.hr_inicio ) AS dtdispo, atividade.hr_inicio, atividade.hr_termino, tipo_nt4.id AS tipo, atividade.cod_decs AS decs FROM presenca INNER JOIN pessoa ON presenca.pessoa = pessoa.id INNER JOIN prof_geral ON prof_geral.pessoa = presenca.pessoa INNER JOIN atividade ON presenca.atividade = atividade.id LEFT JOIN avaliacoes ON presenca.id = avaliacoes.presenca LEFT JOIN prof_saude ON prof_saude.pessoa = presenca.pessoa LEFT JOIN tipo ON atividade.tipo = tipo.id LEFT JOIN tipo_nt4 ON tipo.tipo_nt4 = tipo_nt4.id WHERE atividade.dt != '' AND atividade.dt BETWEEN '${params.dt_i}' AND '${params.dt_f}' ORDER BY atividade.id DESC; SELECT atividade.id AS id_atividade, CONCAT( atividade.dt, ' ', atividade.hr_inicio ) AS dtparti, equipe.ubs AS cnes, avaliacoes.pergunta AS satisf, prof_geral.cbo AS cbo, pessoa.cpf AS cpf, pessoa.id as id, equipe.codigo_ine AS ine FROM presenca INNER JOIN pessoa ON presenca.pessoa = pessoa.id INNER JOIN prof_geral ON prof_geral.pessoa = presenca.pessoa INNER JOIN atividade ON presenca.atividade = atividade.id LEFT JOIN avaliacoes ON presenca.id = avaliacoes.presenca LEFT JOIN prof_saude ON prof_saude.pessoa = presenca.pessoa LEFT JOIN equipe ON prof_saude.equipe = equipe.id LEFT JOIN ubs ON equipe.ubs = ubs.cnes LEFT JOIN tipo ON atividade.tipo = tipo.id LEFT JOIN tipo_nt4 ON tipo.tipo_nt4 = tipo_nt4.id`

    return executaQuery(sql).then(dados => {
      const atividades = dados[0]
      const participantes = dados[1]

      return atividades.map(atividades_teleeducacao => {
        const participantesAtividade = participantes.filter(participante => participante.id_atividade === atividades_teleeducacao.id)

        return ({
          ...atividades_teleeducacao,
          participacoes_teleeducacao: participantesAtividade
        })
      })
    })
  }

}

module.exports = new Atividade


*/