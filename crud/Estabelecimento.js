const executaQuery = require('../database/queries_sints')

class Estabelecimento {
  lista(res) {
    const sql = 'SELECT * FROM ubs'

    return executaQuery(sql).then(dados => {
      const objetos = dados
  
      return objetos.map(objeto => ({
        cnes: objeto.cnes,
        nome: objeto.nome,
        tconsul: 1,
        tdiagn: 1, //verificar
        teduca: 1, //verificar
        
      }))
    })
  }

}

module.exports = new Estabelecimento
