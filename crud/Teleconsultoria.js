const executaQuery = require('../database/queries_sints')

class Teleconsultoria {
  lista() {
    const sql = 'SELECT * FROM objeto_aprendizagem'

    return executaQuery(sql).then(dados => {
        const objetos = dados
    
        return objetos.map(objeto => ({
          dtsol:"05\/01\/2016 18:00:00",
          tipo:"A",
          canal:"2",
          scpf:"00000000000",
          scbo:"000000",
          scnes:"0000000",
          stipo:"01",
          cids:[
            "a010",
            "a040",
            "w25"
          ],
          ciaps:[
            "R05",
            "A03",
            "R21"
          ],
          dtresp:"05\/01\/2016 18:20:00",
          evenc:"0",
          inenc:"0",
          satisf:"4",
          rduvida:"1",
          psof:"0"
          
        }))
    })
  }

  
}

module.exports = new Teleconsultoria
