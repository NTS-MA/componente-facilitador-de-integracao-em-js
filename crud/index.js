const ObjetoDeAprendizagem = require('./ObjetoDeAprendizagem');
const Atividade = require('./Atividade');
const Estabelecimento = require('./Estabelecimento');
const Teleconsultoria = require('./Teleconsultoria');
const Profissional = require('./Profissional');
const Nucleo = require('./Nucleo');
const Mes = require('./Mes');

module.exports = {
  Teleconsultoria,
  ObjetoDeAprendizagem,
  Atividade,
  Estabelecimento,
  Profissional,
  Nucleo,
  Mes,

}
