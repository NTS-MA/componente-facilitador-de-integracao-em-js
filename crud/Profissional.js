const executaQuery = require('../database/queries_sofia')

class Profissional {
  
  lista() {
    
    const sql = 'SELECT `people`.`name` AS nome, `units`.`cnes` , `cbo`.`code` AS `cbo` , `people`.`cpf` , `people`.`sex` , `teams`.`ine` FROM `profiles` INNER JOIN `users` ON `profiles`.`user_id` = `users`.`id` INNER JOIN `people` ON `people`.`id` = `users`.`person_id` INNER JOIN `profiles_teams` ON `profile_id` = `profiles`.`id` INNER JOIN `teams` ON `teams`.`id` = `profiles_teams`.`team_id` INNER JOIN `units` ON `units`.`id` = `teams`.`unit_id` INNER JOIN `cbo` ON `cbo`.`id` = `profiles`.`cbo_id` WHERE `profiles`.`status_id` =1'
    console.log(sql);

    return executaQuery(sql).then(profissionais => {
      
      const objetos = profissionais
  
        return objetos.map(objeto => ({
          tprof: "01",
          nome: objeto.nome,
          cns: objeto.cns,
          cbo: objeto.cbo,
          cpf: objeto.cpf,
          sexo: objeto.sexo,
          ine: objeto.ine,
          //teste: "huehuehue"
                   
        }))
        
    })
  }

  
}

module.exports = new Profissional
