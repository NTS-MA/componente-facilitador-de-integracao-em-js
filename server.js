const { GraphQLServer } = require('graphql-yoga')

const conexao = require('./conexao_sints')

const Operacoes = require('./operations')

const ObjetosDeAprendizagem = new Operacoes('ObjetoDeAprendizagem');

const Atividades = new Operacoes('Atividade');

const Estabelecimentos = new Operacoes('Estabelecimento');

const Teleconsultorias = new Operacoes('Teleconsultoria');

const Profissionais = new Operacoes('Profissional');

const Nucleo = new Operacoes('Nucleo');

const Mes = new Operacoes('Mes');


//const context = require ('./context');

const resolvers = {
  Query: {
    objetosDeAprendizagem: () => ObjetosDeAprendizagem.lista(),
    atividades_teleeducacao: (root, params) => Atividades.lista(params),
    
    estabelecimentos: () => Estabelecimentos.lista(),
    teleconsultorias: () => Teleconsultorias.lista(),
    profissionais: () => Profissionais.lista(),
    codigo_nucleo: () => "0000010",
    mes_referencia: (root, params) => params.mes
    
  }
}

const servidor = new GraphQLServer({
  resolvers,
  typeDefs: './schema.graphql',
  //context
})

servidor.start(() => console.log('O servidor está executando -->>> http://localhost:4000'))

conexao.connect(erro => {
  if (erro) {
    console.log(erro)
  }

  console.log('O banco de dados está executando...')

})

